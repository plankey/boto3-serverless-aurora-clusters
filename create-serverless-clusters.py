import csv

import boto3
from pwgen import pwgen

client = boto3.client('rds', region_name='us-east-1')

new_dbs = {'saas{:0>10}'.format(db): '' for db in range(1)}

for db in new_dbs.keys():
    new_dbs[db] = pwgen(20)
    client.create_db_cluster(
        AvailabilityZones=[
            "us-east-1b",
            "us-east-1a"
        ],
        BackupRetentionPeriod=1,
        DBClusterIdentifier=db,
        VpcSecurityGroupIds=[
            'sg-0ae56de6e47499208'
        ],
        DBSubnetGroupName="default-vpc-0e4903fa92cd92256",
        Engine="aurora",
        MasterUsername="root",
        MasterUserPassword=new_dbs[db],
        StorageEncrypted=True,
        EngineMode="serverless",
        ScalingConfiguration={
            "MinCapacity": 2,
            "MaxCapacity": 64,
            "AutoPause": True,
            "SecondsUntilAutoPause": 300
        },
        DeletionProtection=False
    )

with open('newdbs.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    [writer.writerow(db) for db in new_dbs.items()]
