# Deploy to AWS Aurora
An example script and configuration for deploying a cluster of serverless Aurora instances

# Required Environment Variables
- AWS_SECRET_ACCESS_KEY: Secret key for a user with the required permissions.
- AWS_ACCESS_KEY_ID: Access key for a user with the required permissions.
- AWS_DEFAULT_REGION: Region where the target AWS Lambda function is.

# Original Source
Stole from https://github.com/calvinhp/2019_AWS_Boto3_vBrownBag during his vBrownBag which can be found here https://www.youtube.com/watch?v=MoMfDNdV1Ak